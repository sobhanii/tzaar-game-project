package models;

import java.util.ArrayList;

public abstract class Player {

    private final PlayerType type;

    @Override
    public String toString() {
        return "Player{" +
                "type=" + type +
                '}';
    }

    public Player(PlayerType type) {
        this.type = type;
    }

    public PlayerType getType() {
        return type;
    }

    //the first action should be attack always
    public abstract Action forceAttack(Game game);

    //second action can be attack, reinforce or nothing
    public abstract Action secondAction(Game game);

    public ArrayList<Action> getAllActions(Board board) {
        ArrayList<Action> actions = new ArrayList<>();
        for (Board.BoardRow row : board.getRows()) {
            for (Board.BoardCell cell : row.boardCells) {
                if (cell != null && cell.bead != null && cell.bead.getPlayer().getType() == type) {
                    actions.addAll(cell.bead.getActions(cell));
                }
            }
        }
        actions.add(new Action(Action.ActionType.nothing, null, null));
        return actions;
    }

    public int getAttacks(Board board) {
        int attacks = 0;
        ArrayList<Action> actions = new ArrayList<>();
        for (Board.BoardRow row : board.getRows()) {
            for (Board.BoardCell cell : row.boardCells) {
                if (cell != null && cell.bead != null && cell.bead.getPlayer().getType() == type) {
                    actions.addAll(cell.bead.getActions(cell));
                }
            }
        }
        for (Action action : actions){
            if (action.getType() == Action.ActionType.attack){
                attacks++;
            }
        }
        return attacks;
    }

    public int getOpAttacks(Board board) {
        int attacks = 0;
        ArrayList<Action> actions = new ArrayList<>();
        for (Board.BoardRow row : board.getRows()) {
            for (Board.BoardCell cell : row.boardCells) {
                if (cell != null && cell.bead != null && cell.bead.getPlayer().getType().reverse() == type) {
                    actions.addAll(cell.bead.getActions(cell));
                }
            }
        }
        for (Action action : actions){
            if (action.getType() == Action.ActionType.attack){
                attacks++;
            }
        }
        return attacks;
    }

    public ArrayList<Action> getOpAllActions(Board board) {
        ArrayList<Action> actions = new ArrayList<>();
        for (Board.BoardRow row : board.getRows()) {
            for (Board.BoardCell cell : row.boardCells) {
                if (cell != null && cell.bead != null && cell.bead.getPlayer().getType().reverse() == type) {
                    actions.addAll(cell.bead.getActions(cell));
                }
            }
        }
        actions.add(new Action(Action.ActionType.nothing, null, null));
        return actions;
    }




}
