import models.*;

import java.util.ArrayList;

import static models.BeadType.*;

public class AlphaBeta2 extends Player{

        private int doneActions = 0;
        private final int maxDepth = 2;

        static int alpha = Integer.MIN_VALUE;
        static int beta = Integer.MAX_VALUE;

        public AlphaBeta2(PlayerType type) {
            super(type);
        }

        @Override
        public Action forceAttack(Game game) {
            int maxValue = Integer.MIN_VALUE;
            Action bestAction = null;
            ArrayList<Action> actions = getAllActions(game.getBoard());
            if (doneActions == 0 && getType() == PlayerType.white) {
                for (Action action : actions) {
                    if (action.getType() == Action.ActionType.attack) {
                        Game copyGame = game.copy();
                        if (copyGame.applyActionTwo(this, action, true)) {
                            continue;
                        }
                        Player winner = copyGame.getWinner();
                        if (winner != null) {
                            if (winner.getType() == getType()) {
                                return action;
                            }
                        } else {
                            int temp = Math.max(maxValue, alphaBetaMinForceAttack(copyGame, 0));
                            if (temp > maxValue) {
                                maxValue = temp;
                                bestAction = action;
                            }
                        }
                    }
                }
            } else {
                for (Action action : actions) {
                    if (action.getType() == Action.ActionType.attack) {
                        Game copyGame = game.copy();
                        if (copyGame.applyActionTwo(this, action, true)) {
                            continue;
                        }
                        Player winner = copyGame.getWinner();
                        if (winner != null) {
                            if (winner.getType() == getType()) {
                                return action;
                            }
                        } else {
                            int temp = Math.max(maxValue, alphaBetaMaxSecondMove(copyGame, 0));
                            if (temp > maxValue) {
                                maxValue = temp;
                                bestAction = action;
                            }
                        }
                    }
                }
            }
            doneActions++;
            return bestAction;
        }

        //my actions - oponnent actions
        private int actionDifferences(Game game){
            int myActions = this.getAllActions(game.getBoard()).size();
            int opActions = this.getOpAllActions(game.getBoard()).size();
            return myActions - opActions;
        }

        private int attackDifferences(Game game){
            return this.getAttacks(game.getBoard()) - this.getOpAttacks(game.getBoard());
        }

        // my bead relations - oponnent bead relations
        private int beadScale(Game game){
            int scalePoint = 0;

            int myPoint = 0;
            int opPoint = 0;
            int myBead1 = 0;
            int myBead2 = 0;
            int myBead3 = 0;
            int opBead1 = 0;
            int opBead2 = 0;
            int opBead3 = 0;
            Bead[] gameBeads;
            Board.BoardRow[] brs = game.getBoard().getRows();
            for (int i = 0; i < 9; i++) {
                Board.BoardRow br = brs[i];
                Board.BoardCell[] brCells = br.boardCells;
                for (int j = 0; j < brCells.length; j++) {
                    Board.BoardCell bc = brCells[j];
                    //height check, player check
                    if (bc.bead != null && bc.bead.getPlayer().getType() == this.getType()) {
                        int h = bc.bead.getHeight();
                        Player pl = bc.bead.getPlayer();
                        BeadType bt = bc.bead.getType();
                        if (bt == Tzaars) {
                            myBead1++;
                        }
                        if (bt == Tzarras) {
                            myBead2++;
                        }
                        if (bt == Totts) {
                            myBead3++;
                        }
                    }
                    else if (bc.bead != null && bc.bead.getPlayer().getType() == this.getType().reverse()){
                        BeadType bt1 = bc.bead.getType();
                        if (bt1 == Tzaars) {
                            opBead1++;
                        }
                        if (bt1 == Tzarras) {
                            opBead2++;
                        }
                        if (bt1 == Totts) {
                            opBead3++;
                        }
                    }
                }
            }
            myPoint = (myBead1 / 6) + (myBead2 / 9) + (myBead3 / 15);
            opPoint = (opBead1 / 6) + (opBead2 / 9) + (opBead3 / 15);
            scalePoint = myPoint - opPoint;
            return scalePoint;
        }

        // my bead sum - oponnet bead sum
        private int sumDifference(Game game){
            int sumDiff = 0;
            int myBeads = 0;
            int opBeads = 0;
            Bead[] gameBeads;
            Board.BoardRow[] brs = game.getBoard().getRows();
            for (int i = 0; i < 9; i++) {
                Board.BoardRow br = brs[i];
                Board.BoardCell[] brCells = br.boardCells;
                for (int j = 0; j < brCells.length; j++) {
                    Board.BoardCell bc = brCells[j];
                    //height check, player check
                    if (bc.bead != null && bc.bead.getPlayer().getType() == this.getType()) {
                        int h = bc.bead.getHeight();
                        for (int k=0; k<h; k++){
                            myBeads++;
                        }
                    }
                    else if (bc.bead != null && bc.bead.getPlayer().getType() == this.getType().reverse()){
                        int h1 = bc.bead.getHeight();
                        for (int k=0; k<h1; k++){
                            opBeads++;
                        }

                    }
                }
            }
            sumDiff = myBeads - opBeads;
            return sumDiff;
        }



        private int eval(Game game) {
            int evalPoint = 0;
            int actionsDiff = actionDifferences(game) * 250;
            int attackDiff = attackDifferences(game) * 450;
            int beadScale = beadScale(game) * 200;
            int sumDiff = sumDifference(game) * 100;
            evalPoint = actionsDiff + attackDiff + beadScale + sumDiff;
            return evalPoint;
        }
        private int eval2(Game game){
            int scalePoint = 0;

            int myPoint = 0;
            int opPoint = 0;
            int myBead1 = 0;
            int myBead2 = 0;
            int myBead3 = 0;

            Bead[] gameBeads;
            Board.BoardRow[] brs = game.getBoard().getRows();
            for (int i = 0; i < 9; i++) {
                Board.BoardRow br = brs[i];
                Board.BoardCell[] brCells = br.boardCells;
                for (int j = 0; j < brCells.length; j++) {
                    Board.BoardCell bc = brCells[j];
                    //height check, player check
                    if (bc.bead != null && bc.bead.getPlayer().getType() == this.getType()) {
                        int h = bc.bead.getHeight();
                        Player pl = bc.bead.getPlayer();
                        BeadType bt = bc.bead.getType();
                        if (bt == Tzaars) {
                            myBead1++;
                        }
                        if (bt == Tzarras) {
                            myBead2++;
                        }
                        if (bt == Totts) {
                            myBead3++;
                        }
                    }

                }
            }
            myPoint = (myBead1 / 6) + (myBead2 / 9) + (myBead3 / 15);
            scalePoint = myPoint;
            return scalePoint;
        }

        @Override
        public Action secondAction(Game game) {
            int maxValue = Integer.MIN_VALUE;
            Action bestAction = null;
            ArrayList<Action> actions = getAllActions(game.getBoard());
            for (Action action : actions) {
                Game copyGame = game.copy();
                if (copyGame.applyActionTwo(this, action, false)) {
                    continue;
                }
                Player winner = copyGame.getWinner();
                if (winner != null) {
                    if (winner.getType() == getType()) {
                        return action;
                    }
                } else {
                    int temp = Math.max(maxValue, alphaBetaMinForceAttack(copyGame, 0));
                    if (temp > maxValue) {
                        maxValue = temp;
                        bestAction = action;
                    }
                }
            }
            doneActions++;
            return bestAction;
        }



//    private int maxForceAttack(Game game, int depth) {
//        if (depth == maxDepth) {
//            return eval(game);
//        }
//
//        int maxValue = Integer.MIN_VALUE;
//        ArrayList<Action> actions = getAllActions(game.getBoard());
//        for (Action action : actions) {
//            if (action.getType() == Action.ActionType.attack) {
//                Game copyGame = game.copy();
//                if (copyGame.applyActionTwo(this, action, true)) {
//                    continue;
//                }
//                Player winner = copyGame.getWinner();
//                if (winner != null) {
//                    if (winner.getType() == getType()) {
//                        return Integer.MAX_VALUE;
//                    }
//
//                }
//                else {
//                    maxValue = Math.max(maxValue, maxSecondMove(copyGame, depth + 1));
//                }
//            }
//        }
//        return maxValue;
//    }

        private int alphaBetaMaxForceAttack(Game game, int depth) {

            if (depth == maxDepth) {
                return eval(game);
            }
            int maxValue = Integer.MIN_VALUE;
            int best = alpha;
            ArrayList<Action> actions = getAllActions(game.getBoard());
            for (Action action : actions) {
                if (action.getType() == Action.ActionType.attack) {
                    Game copyGame = game.copy();
                    if (copyGame.applyActionTwo(this, action, true)) {
                        continue;
                    }
                    Player winner = copyGame.getWinner();
                    if (winner != null) {
                        if (winner.getType() == getType()) {
                            return Integer.MAX_VALUE;
                        }
                    }
                    else {
                        maxValue = Math.max(maxValue, alphaBetaMaxSecondMove(copyGame, depth + 1));
                        best = Math.max(best, maxValue);
                        alpha = Math.max(alpha, best);

                        if (beta <= alpha){
                            break;
                        }

                    }
                }

            }
            return best;
        }


//    private int maxSecondMove(Game game, int depth) {
//        if (depth == maxDepth) {
//            return eval(game);
//        }
//
//        int maxValue = Integer.MIN_VALUE;
//        ArrayList<Action> actions = getAllActions(game.getBoard());
//        for (Action action : actions) {
//            Game copyGame = game.copy();
//            if (copyGame.applyActionTwo(this, action, false)) {
//                continue;
//            }
//            Player winner = copyGame.getWinner();
//            if (winner != null) {
//                if (winner.getType() == getType()) {
//                    return Integer.MAX_VALUE;
//                }
//                else {
//                    return Integer.MIN_VALUE;
//                }
//            }
//            else {
//                maxValue = Math.max(maxValue, minForceAttack(copyGame, depth + 1));
//            }
//        }
//        return maxValue;
//    }

        private int alphaBetaMaxSecondMove(Game game, int depth) {


            if (depth == maxDepth) {
                return eval(game);
            }

            int maxValue = Integer.MIN_VALUE;
            int best = alpha;

            ArrayList<Action> actions = getAllActions(game.getBoard());
            for (Action action : actions) {
                Game copyGame = game.copy();
                if (copyGame.applyActionTwo(this, action, false)) {
                    continue;
                }
                Player winner = copyGame.getWinner();
                if (winner != null) {
                    if (winner.getType() == getType()) {
                        return Integer.MAX_VALUE;
                    }
                    else {
                        return Integer.MIN_VALUE;
                    }
                }
                else {
                    maxValue = Math.max(maxValue, alphaBetaMinForceAttack(copyGame, depth + 1));
                    best = Math.max(best, maxValue);
                    alpha = Math.max(alpha, best);

                    if (beta <= alpha){
                        break;
                    }
                }
            }
            return best;
        }

//    private int minForceAttack(Game game, int depth) {
//        if (depth == maxDepth) {
//            return eval(game);
//        }
//
//        int minValue = Integer.MAX_VALUE;
//        ArrayList<Action> actions = getAllActions(game.getBoard());
//        for (Action action : actions) {
//            if (action.getType() == Action.ActionType.attack) {
//                Game copyGame = game.copy();
//                if (copyGame.applyActionTwo(this, action, true)) {
//                    continue;
//                }
//                Player winner = copyGame.getWinner();
//                if (winner != null) {
//                    if (winner.getType() == getType().reverse()) {
//                        return Integer.MIN_VALUE;
//                    }
//                } else {
//                    minValue = Math.min(minValue, minSecondMove(copyGame, depth + 1));
//                }
//            }
//        }
//        return minValue;
//
//    }

        private int alphaBetaMinForceAttack(Game game, int depth) {


            if (depth == maxDepth) {
                return eval(game);
            }

            int minValue = Integer.MAX_VALUE;
            int best = beta;

            ArrayList<Action> actions = getAllActions(game.getBoard());
            for (Action action : actions) {
                if (action.getType() == Action.ActionType.attack) {
                    Game copyGame = game.copy();
                    if (copyGame.applyActionTwo(this, action, true)) {
                        continue;
                    }
                    Player winner = copyGame.getWinner();
                    if (winner != null) {
                        if (winner.getType() == getType().reverse()) {
                            return Integer.MIN_VALUE;
                        }
                    } else {
                        minValue = Math.min(minValue, alphaBetaMinSecondMove(copyGame, depth + 1));
                        best = Math.min(best, minValue);
                        beta = Math.min(beta, best);

                        if (beta <= alpha){
                            break;
                        }
                    }
                }
            }
            return best;
        }


//    private int minSecondMove(Game game, int depth) {
//        if (depth == maxDepth) {
//            return eval(game);
//        }
//
//        int minValue = Integer.MAX_VALUE;
//        ArrayList<Action> actions = getAllActions(game.getBoard());
//        for (Action action : actions) {
//            Game copyGame = game.copy();
//            if (copyGame.applyActionTwo(this, action, false)) {
//                continue;
//            }
//            Player winner = copyGame.getWinner();
//            if (winner != null) {
//                if (winner.getType() == getType().reverse()) {
//                    return Integer.MIN_VALUE;
//                }else {
//                    return Integer.MAX_VALUE;
//                }
//            } else {
//                minValue = Math.min(minValue, maxForceAttack(copyGame, depth + 1));
//            }
//        }
//
//        return minValue;
//    }


        private int alphaBetaMinSecondMove(Game game, int depth) {


            if (depth == maxDepth) {
                return eval(game);
            }

            int minValue = Integer.MAX_VALUE;
            int best = beta;

            ArrayList<Action> actions = getAllActions(game.getBoard());
            for (Action action : actions) {
                Game copyGame = game.copy();
                if (copyGame.applyActionTwo(this, action, false)) {
                    continue;
                }
                Player winner = copyGame.getWinner();
                if (winner != null) {
                    if (winner.getType() == getType().reverse()) {
                        return Integer.MIN_VALUE;
                    }else {
                        return Integer.MAX_VALUE;
                    }
                } else {
                    minValue = Math.min(minValue, alphaBetaMaxForceAttack(copyGame, depth + 1));
                    best = Math.min(best, minValue);
                    beta = Math.min(beta, best);

                    if (beta <= alpha){
                        break;
                    }
                }
            }
            return best;
        }
    }

